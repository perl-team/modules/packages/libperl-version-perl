Source: libperl-version-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libscalar-list-utils-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libperl-version-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libperl-version-perl.git
Homepage: https://metacpan.org/release/Perl-Version
Rules-Requires-Root: no

Package: libperl-version-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libscalar-list-utils-perl
Description: module to parse and manipulate Perl version strings
 Perl::Version provides a simple interface for parsing, manipulating and
 formatting Perl version strings.
 .
 Unlike version.pm (which concentrates on parsing and comparing version
 strings) Perl::Version is designed for cases where you'd like to parse a
 version, modify it and get back the modified version formatted like the
 original.
